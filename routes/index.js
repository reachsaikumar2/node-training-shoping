var express = require('express');
var router = express.Router();
// const csv = require('csv-parser');
// const fs = require('fs');
// fs.createReadStream('data.csv')
// .pipe(csv())
// .on('data', (row) => {
//   var list = row;
//   console.log(row);
// })
// .on('end', () => {
//   console.log('CSV file successfully processed');
// });

/* GET home page. */
router.get('/', function(req, res, next) {

  res.render('shop/index', { title: 'Shopping Cart'});
});

module.exports = router;

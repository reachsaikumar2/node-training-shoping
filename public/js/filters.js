var data = [{
        id: 1, name: 'sample prod 1', color: 'red', price: 2000, type: '3g'
    },
    {
        id: 2, name: 'another prod', color: 'red', price: 2000, type: '3g'
    },
    {
        id: 3, name: 'testing 123', color: 'green', price: 2000, type: '4g'
    },
    {
        id: 4, name: 'refrigirator', color: 'yellow', price: 2000, type: '5g'
    },
    {
        id: 5, name: 'washing machine', color: 'green', price: 2000, type: '3g'
    }];
    var orginalData = data;
    var filterdData = [];
    var searchHistory = [];
    var filters = {
        color: [],
        type: []
    };
    var i = 1;
    addProductsHtml(data);
    function getFilters(csvData = data) {
        csvData = csvData.filter(item => item && item);
        document.getElementById('filtersId').innerHTML = '';
        let categories = ['color', 'type'];
        categories.forEach(category => {
            if(category) {
            var divElem = document.createElement('div');
            var paragraph = document.createElement("P");
            var text = document.createTextNode(category);
            paragraph.appendChild(text);
            divElem.appendChild(paragraph);
            document.getElementById('filtersId').appendChild(divElem);
            let filterData = csvData.reduce((acc, o) => o && (acc[o[category]] = (acc[o[category]] || 0) + 1, acc), {});
            Object.keys(filterData).forEach((item, index) => addHTml({ value: item, id: filterData[item], type: category }));
            }
        })
    }

    function test(value, type, status) {
        document.getElementById(value).checked = status;
        if (status) {
            filters[type].push(value);

        } else {
            index = filters[type].findIndex(test => test === value);
            filters[type].splice(index, 1);
        }
        console.log('filters', filters);
        // filterProducts();
    }
    function clearFilters() {
        filters = {
            color: [],
            type: []
        };
        filterProducts();
        getFilters();
        searchHistory = [];
        renderSearchHistory();
    }
    function filterProducts(dontAddToHistory = false) {
        if(!dontAddToHistory) {
            searchHistory.push(JSON.stringify(filters));
        }
        var filterdData = Object.keys(filters).map(filter => {
            if (filters[filter].length > 0) {
                return filters[filter].map(item => {
                    return data.filter((arr, index) => {
                        if (arr[filter] == item) {
                            return arr;
                        }
                    })
                });
            }
        });
        if(filterdData[0] === undefined) {
            addProductsHtml(data);
            // getFilters(data);
        } else {
            addProductsHtml(filterdData.flat(2));
            // getFilters(filterdData.flat(2));
        }
        renderSearchHistory();
    }

    function renderSearchHistory() {
        document.getElementById("list").innerHTML = '<ul id="list"></ul>';
        searchHistory.forEach(item => {
            var ul = document.getElementById("list");
            var li = document.createElement("li");
            li.setAttribute('id', item);     // SET UNIQUE ID.
            li.addEventListener('click', (e) => {
                filterHistory(e.target.id);
            });
            li.appendChild(document.createTextNode(item));
            ul.appendChild(li);
        });
    }

    function addProductsHtml(filterd) {
        var container = document.getElementById('product');
        let htmlData  = '';
        filterd && filterd.forEach(item => {
            if(item && item.name) {
                let sample = `<div class="col-sm-6 col-md-4" id="product">
                                <div class="thumbnail">
                                    <img src="https://upload.wikimedia.org/wikipedia/en/5/5e/Gothiccover.png" alt="..." class="img-responsive">
                                    <div class="caption">
                                        <h3>${item.name}</h3>
                                        <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A consequuntur debitis delectus deserunt dolore dolorem eum expedita, inventore nobis omnis perferendis possimus quas repellat soluta, sunt voluptate, voluptates! Assumenda, nulla?</p>
                                        <div class="clearfix">
                                            <div class="price pull-left">$12</div>
                                            <a href="#" class="btn btn-success pull-right" role="button">Button</a>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                htmlData += sample;
            }
 
        })        
        container.innerHTML = htmlData;
    }




    function addHTml(obj = { value: 123, id: 1, type: 'color' }) {
        var container = document.getElementById('filtersId');
        if (obj.value !== '') {

            var chk = document.createElement('input');  // CREATE CHECK BOX.
            chk.setAttribute('type', 'checkbox');       // SPECIFY THE TYPE OF ELEMENT.
            chk.setAttribute('id', obj.value);     // SET UNIQUE ID.
            chk.setAttribute('value', obj.value);
            chk.setAttribute('name', obj.type);
            chk.addEventListener('click', (e, obj) => {
                test(e.target.value, e.target.name, e.target.checked);
            });

            var lbl = document.createElement('label');  // CREATE LABEL.
            lbl.setAttribute('for', obj.value + obj.id);

            // CREATE A TEXT NODE AND APPEND IT TO THE LABEL.
            lbl.appendChild(document.createTextNode(obj.value + '(' + obj.id + ')'));

            var divElem = document.createElement('div');
            divElem.appendChild(chk);
            divElem.appendChild(lbl);

            // APPEND THE NEWLY CREATED CHECKBOX AND LABEL TO THE <p> ELEMENT.
            container.appendChild(divElem);

            obj.value = '';

            i = i + 1;
        }
    }

    function filterHistory(value) {
        filters = JSON.parse(value);
        filterProducts(true);
        getFilters();
    }